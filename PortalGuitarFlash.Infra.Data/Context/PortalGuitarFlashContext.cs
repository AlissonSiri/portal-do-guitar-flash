﻿using PortalGuitarFlash.DDD.Entities;
using PortalGuitarFlash.Infra.Data.EntityConfig;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalGuitarFlash.Infra.Data.Context
{
    public class PortalGuitarFlashContext: DbContext
    {

        public PortalGuitarFlashContext(): base("PortalGuitarFlash")
        {
            this.Configuration.LazyLoadingEnabled = true;
        }

        DbSet<User> Users { get; set; }
        DbSet<ExternalSong> ExternalSongs { get; set; }
        DbSet<DownloadedSong> DownloadedSongs { get; set; }
        DbSet<Author> Authors { get; set; }
        DbSet<ChangeLog> ChangeLogs { get; set; }
        DbSet<UserProfileImage> UserProfileImages { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Properties<int>().Where(p => p.Name == "ID").Configure(p => p.IsKey());

            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            modelBuilder.Properties<string>().Configure(p => p.HasColumnType("nvarchar"));
            modelBuilder.Properties<string>().Configure(p => p.HasMaxLength(100));
            modelBuilder.Properties<DateTime>().Configure(p => p.HasColumnType("smalldatetime"));

            modelBuilder.Configurations.Add(new UserConfiguration());
            modelBuilder.Configurations.Add(new ExternalSongConfiguration());
            modelBuilder.Configurations.Add(new DownloadedSongConfiguration());
            modelBuilder.Configurations.Add(new AuthorConfiguration());
            modelBuilder.Configurations.Add(new UserProfileImageConfiguration());
        }

        public override int SaveChanges()
        {
            foreach (DbEntityEntry entry in ChangeTracker.Entries().Where(e => e.Entity.GetType().GetProperty("Active") != null))
            {
                if (entry.State == EntityState.Added)
                    entry.Property("Active").CurrentValue = true;
            }
            foreach (DbEntityEntry entry in ChangeTracker.Entries().Where(e => e.Entity.GetType().GetProperty("RegisterDate") != null))
            {
                if (entry.State == EntityState.Added)
                    entry.Property("RegisterDate").CurrentValue = DateTime.UtcNow;
                else
                    entry.Property("RegisterDate").IsModified = false;
            }
            foreach (DbEntityEntry entry in ChangeTracker.Entries().Where(e => e.Entity.GetType().GetProperty("UpdateDate") != null))
            {
                if (entry.State == EntityState.Added || entry.State == EntityState.Modified)
                    entry.Property("UpdateDate").CurrentValue = DateTime.UtcNow;
                else
                    entry.Property("UpdateDate").IsModified = false;
            }
            return base.SaveChanges();
        }

    }
}
