﻿using PortalGuitarFlash.DDD.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalGuitarFlash.Infra.Data.EntityConfig
{
    public class UserConfiguration: EntityTypeConfiguration<User>
    {

        public UserConfiguration()
        {
            Property(p => p.Name).IsRequired().HasMaxLength(255);

            Property(p => p.Description).HasMaxLength(4000);

            HasMany<ExternalSong>(u => u.ExternalSongs);

            HasMany<UserProfileImage>(u => u.Images);

            Property(p => p.Email).IsRequired()
            .HasMaxLength(255)
            .HasColumnAnnotation(
                IndexAnnotation.AnnotationName,
                new IndexAnnotation(
                    new IndexAttribute("IX_User_Email", 2) { IsUnique = true }));
        }

    }
}
