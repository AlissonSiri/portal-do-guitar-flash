﻿using PortalGuitarFlash.DDD.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalGuitarFlash.Infra.Data.EntityConfig
{
    public class ExternalSongConfiguration: EntityTypeConfiguration<ExternalSong>
    {

        public ExternalSongConfiguration()
        {
            Property(p => p.Name).IsRequired();
            HasRequired(p => p.Owner);
            HasRequired(p => p.Author);
            Property(p => p.VideoUrl).HasMaxLength(255);
            this.Ignore(p => p.DownloadCount);
        }

    }
}
