﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalGuitarFlash.DDD.Entities
{
    public class ExternalSong
    {
        private int _downloadCount = 0;

        public int ID { get; protected set; }
        public virtual User Owner { get; protected set; }
        public virtual Author Author { get; protected set; }
        public string Name { get; set; }
        public float Level { get; set; }
        public string DownloadUrl { get; set; }
        public string VideoUrl { get; set; }
        public bool Active { get; protected set; }
        public DateTime RegisterDate { get; protected set; }
        public DateTime UpdateDate { get; protected set; }
        public virtual ICollection<DownloadedSong> DownloadedSongs { get; protected set; }
        public int DownloadCount {
            get {
                if (_downloadCount == 0)
                    _downloadCount = DownloadedSongs.Count();
                return _downloadCount;
            }
        }

        public void SetDownloadCount(int count)
        {
            this._downloadCount = count;
        }

        public EmbeddedVideo GetEmbedded()
        {
            return new EmbeddedVideo(this.VideoUrl);
        }

    }
}
