﻿using PortalGuitarFlash.DDD.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalGuitarFlash.DDD.Entities
{
    public class UserProfileImage
    {

        public int ID { get; private set; }
        public virtual User Owner { get; protected set; }
        public byte[] Image { get; set; }
        public ImageType Type { get; set; }

    }
}
