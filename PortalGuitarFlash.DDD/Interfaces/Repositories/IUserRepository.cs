﻿using PortalGuitarFlash.DDD.Entities;

namespace PortalGuitarFlash.DDD.Interfaces.Repositories
{
    public interface IUserRepository: IBaseRepository<User>
    {

        User GetByEmail(string email);

    }
}
