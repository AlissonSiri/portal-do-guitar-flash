﻿using PortalGuitarFlash.DDD.Entities;
using System.Collections.Generic;

namespace PortalGuitarFlash.DDD.Interfaces.Repositories
{
    public interface IExternalSongRepository: IBaseRepository<ExternalSong>
    {

        IEnumerable<ExternalSong> FindByName(string name);

    }
}
