﻿using AutoMapper;
using PortalGuitarFlash.DDD.Entities;
using PortalGuitarFlash.DDD.Helpers;
using PortalGuitarFlash.Web.ViewModels;
using PortalGuitarFlash.Web.ViewModels.Song;
using PortalGuitarFlash.Web.ViewModels.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PortalGuitarFlash.Web.AutoMapper
{

    public class DomainToViewModelMappingProfile: Profile
    {

       public override string ProfileName
        {
            get
            {
                return "ViewModelToDomainMappingProfile";
            }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<User, UserViewModel>();
            Mapper.CreateMap<ExternalSong, ExternalSongViewModel>().IgnoreAllPropertiesWithAnInaccessibleSetter();
            Mapper.CreateMap<ChangeLog, ChangeLogViewModel>();
            Mapper.CreateMap<Author, AuthorViewModel>();
            //Mapper.CreateMap<CollectionPage<ExternalSong>, CollectionPageViewModel<ExternalSongViewModel>>();101215049286
            Mapper.CreateMap(typeof(CollectionPage<>), typeof(CollectionPageViewModel<>))
                .ForMember("Pagina", opt => opt.MapFrom("CurrentPage"))
                .ForMember("Quantidade", opt => opt.MapFrom("ItemsPerPage"))
                .ForMember("Texto", opt => opt.MapFrom("SearchBuilder.SearchText"));

            Mapper.CreateMap<User, ProfileViewModel>()
                .ForMember(p => p.Age, opt => opt.MapFrom(u => u.GetAge().HasValue ? String.Format("{0} anos", u.GetAge()) : ""));
        }

    }
}