﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PortalGuitarFlash.DDD.Entities;

namespace PortalGuitarFlash.Web.Controllers
{
    [RoutePrefix("image")]
    public class ImageController : Controller
    {
        [Route("user/{userId:int}/profile")]
        public ActionResult UserProfileImage(int userId)
        {
            User user = UserController.repo.GetById(userId);
            UserProfileImage img = user.Images.Where(i => i.Type == DDD.Enum.ImageType.Profile).FirstOrDefault();
            if (img == null)
                return File(Server.MapPath("~/img/unknown-person.gif"), "image/jpg");
            return File(img.Image, "image/jpg");
        }

    }
}