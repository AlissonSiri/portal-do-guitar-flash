﻿using PortalGuitarFlash.DDD.Entities;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace PortalGuitarFlash.Web.ViewModels.Song
{
    public class ExternalSongViewModel
    {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        [DisplayName("Charter")]
        public string OwnerName { get; set; }
        public int OwnerID { get; set; }
        [DisplayName("Autor")]
        public string AuthorName { get; set; }
        public float Level { get; set; }
        [DisplayName("Downloads")]
        public int DownloadCount { get; set; }
        [DisplayName("Url")]
        [DataType(DataType.Url)]
        public string DownloadUrl { get; set; }
        [DisplayName("Vídeo")]
        [DataType(DataType.Url)]
        public string VideoUrl { get; set; }
        [DisplayName("Data de Registro")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm}", ApplyFormatInEditMode = true)]
        public DateTime RegisterDate { get; set; }
        public EmbeddedVideo Embedded { get; set; }

        public string GetDownloadDescription()
        {
            string d = this.DownloadCount < 2 ? "Download" : "Downloads";
            return String.Format(String.Concat("{0} ", d), this.DownloadCount);
        }

    }
}