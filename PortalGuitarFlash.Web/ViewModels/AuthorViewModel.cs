﻿using PortalGuitarFlash.Web.ViewModels.Song;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace PortalGuitarFlash.Web.ViewModels
{
    public class AuthorViewModel
    {
        [Key]
        public int ID { get; set; }
        [Required(ErrorMessage = "Por favor, digite um nome!")]
        [MaxLength(100, ErrorMessage = "O nome da banda/autor deve ter até {0} caracteres!")]
        [DisplayName("Nome")]
        public string Name { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm}", ApplyFormatInEditMode = true)]
        public DateTime RegisterDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm}", ApplyFormatInEditMode = true)]
        public DateTime UpdateDate { get; set; }
        public IEnumerable<ExternalSongViewModel> ExternalSongs { get; set; }
    }
}
