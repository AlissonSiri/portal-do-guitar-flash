﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PortalGuitarFlash.Web.Startup))]
namespace PortalGuitarFlash.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
